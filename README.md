I'm having troubles installing node as a local project dependency on armv7l
(raspberry pi 3B+).

My environment:
- Raspbian `2018-11-13-raspbian-stretch-full.img` [from
  raspberrypi.org][raspbian download page]
- Node.js 8.15.0 from [nodesource][nodesource install instructions]

When I run `npm install node@8` in a project I get this error: `cannot find
module node-linux-arm/package.json`
([logs](./logs-and-output/2019-01-05T21_57_44_787Z-debug.log),
[stdout](./logs-and-output/npm-i-node-command-output)).

This makes sense, because that [doesn't exist][node-linux-arm npm search].
However, `node-linux-armv7l` does. Trying to install that via `npm install
node-linux-armv7l@8` fails with `EBADPLATFORM (wanted: armv7l, current: arm)`
([logs](./logs-and-output/2019-01-05T22_00_10_802Z-debug.log),
[stdout](./logs-and-output/npm-i-node-linux-armv7l-command-output)),

Trying to force it with `npm install -f node-linux-armv7l@8` works in this
project, but fails in a more complicated project with an error like this:

```
node@8.12.0 preinstall /home/me/proj/node_modules/node

node installArchSpecificPackage
E404 Not Found: node-linux-arm@8.12.0
```

[node-linux-arm npm search]: https://www.npmjs.com/search?q=node%2Dlinux%2Darm
[nodesource install instructions]: https://github.com/nodesource/distributions/blob/master/README.md#debinstall
[raspbian download page]: https://www.raspberrypi.org/downloads/raspbian/
